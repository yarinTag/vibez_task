import { useState } from "react";
import { useAuthContext } from "./useAuthContext";

export const useEmailConfirmation = () => {
  const [error, setError] = useState(null);
  const [isLoading, setIsLoading] = useState(null);
  const { dispatch } = useAuthContext();

  const emailConfirmation = async (token) => {
    setIsLoading(true);
    setError(null);
    const response = await fetch(
      process.env.REACT_APP_API_URL +
        "/users/confirm?confirmation_token=" +
        token,
      {
        method: "GET",
        headers: { "Content-Type": "application/json" },
      }
    );
    const json = await response.json();

    if (!response.ok) {
      setIsLoading(false);
      setError(json.error.message);
      return false;
    }
    if (response.ok) {
      console.log("LOGIN RES: ", json);
      // save the user to local storage
      localStorage.setItem("token", JSON.stringify(json.token));
      localStorage.setItem("user_data", JSON.stringify(json.user));
      // update the auth context
      dispatch({ type: "LOGIN", payload: json.token });
      // update loading state
      setIsLoading(false);
      return true;
    }
  };

  return { emailConfirmation, isLoading, error };
};
